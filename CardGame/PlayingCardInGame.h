//
//  PlayingCardInGame.h
//  CardGame
//
//  Created by Ilya Rodin on 19.03.14.
//  Copyright (c) 2014 IlyaRodin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardGame.h"

@class PlayingCard;

@interface PlayingCardInGame : NSObject

@property (nonatomic, readonly) PlayingCard *card;
@property (nonatomic, assign) CardStatus status;

- (id)initWithCard:(PlayingCard *)card;

@end
