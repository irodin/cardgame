//
//  CardGame.m
//  CardGame
//
//  Created by Ilya Rodin on 19.03.14.
//  Copyright (c) 2014 IlyaRodin. All rights reserved.
//

#import "CardGame.h"
#import "SynthesizeSingleton.h"
#import "PlayingCard.h"
#import "PlayingCardInGame.h"

NSString *const CardGamePointsDidUpdatedNotification = @"CardGamePointsDidUpdatedNotification";
NSString *const CardGameCardsWereGivenNotification = @"CardGameCardsWereGivenNotification";
NSString *const CardGameCardDidOpenedNotification = @"CardGameCardDidOpenedNotification";
NSString *const CardGameCardsDidMatchedNotification = @"CardGameCardsDidMatchedNotification";
NSString *const CardGameCardsDidClosedNotification = @"CardGameCardsDidClosedNotification";

static const NSUInteger kCardsToGiveCount = 12;

@interface CardGame () {
    NSArray *_cards;
    NSMutableArray *_openedCards;
}

@end

@implementation CardGame

#pragma mark - Init & dealloc

SYNTHESIZE_SINGLETON_FOR_CLASS(CardGame)

#pragma mark -

- (void)setGameType:(CardGameType)gameType
{
    _gameType = gameType;
    
    [self giveCards];
}

- (NSUInteger)cardsToMatchCount
{
    if (CardGameType2Cards == _gameType) {
        return 2;
    } else { // 3 cards
        return 3;
    }
}

- (void)giveCards
{
    NSMutableArray *allCards = [[PlayingCard allCards] mutableCopy];
    
    NSUInteger cardsToGiveCount = kCardsToGiveCount;
    NSMutableArray *givenCards = [[NSMutableArray alloc] initWithCapacity:cardsToGiveCount];
    while (cardsToGiveCount > 0) {
        int randInd = arc4random() % allCards.count;
        
        PlayingCard *card = allCards[randInd];
        
        PlayingCardInGame *cardInGame = [[PlayingCardInGame alloc] initWithCard:card];
        
        [givenCards addObject:cardInGame];
        
        [allCards removeObject:card];
        
        cardsToGiveCount--;
    }
    
    _cards = [NSArray arrayWithArray:givenCards];
    _openedCards = [[NSMutableArray alloc] initWithCapacity:_cards.count];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CardGameCardsWereGivenNotification
                                                        object:nil];
    _points = 0;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CardGamePointsDidUpdatedNotification
                                                        object:nil];
}

- (void)checkForMatching
{
    if (_openedCards.count != [self cardsToMatchCount]) {
        return;
    }
    
    PlayingCard *firstCard = [(PlayingCardInGame *)[_openedCards firstObject] card];
    
    BOOL isSuitMatched = YES;
    BOOL isRankMatched = YES;
    
    for (NSUInteger i = 1; i < _openedCards.count; i++) {
        PlayingCardInGame *openedCard = [_openedCards objectAtIndex:i];
        
        if (openedCard.card.suit != firstCard.suit) {
            isSuitMatched = NO;
        }
        if (openedCard.card.rank != firstCard.rank) {
            isRankMatched = NO;
        }
        if (!isSuitMatched && !isRankMatched) {
            break;
        }
    }
    
    static NSUInteger matchBonusPoints = 4;
    static NSUInteger matchSuitPoints = 1;
    static NSUInteger matchRankPoints = 4;
    
    if (isSuitMatched) {
        _points += matchSuitPoints * matchBonusPoints;
    } else if (isRankMatched) {
        _points += matchRankPoints * matchBonusPoints;
    }
    
    NSMutableArray *openedIndices = [[NSMutableArray alloc] initWithCapacity:_openedCards.count];
    for (PlayingCardInGame *openedCard in _openedCards) {
        [openedIndices addObject:@([_cards indexOfObject:openedCard])];
        openedCard.status = (isSuitMatched || isRankMatched) ? CardStatusMatched : CardStatusClosed;
    }
    [_openedCards removeAllObjects]; // cards will be closed or matched
    
    if (isSuitMatched || isRankMatched) {
        [[NSNotificationCenter defaultCenter] postNotificationName:CardGamePointsDidUpdatedNotification
                                                            object:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CardGameCardsDidMatchedNotification
                                                            object:openedIndices];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:CardGameCardsDidClosedNotification
                                                            object:openedIndices];
    }
}

#pragma mark - Public methods

- (void)restart
{
    [self giveCards];
}

- (NSUInteger)givenCardsCount
{
    return _cards.count;
}

- (CardStatus)statusOfCardWithIndex:(NSUInteger)index
{
    assert(index < _cards.count);
    
    return [(PlayingCardInGame *)_cards[index] status];
}

- (void)openCardWithIndex:(NSUInteger)index
{
    assert(index < _cards.count);
    
    PlayingCardInGame *card = _cards[index];
    
    if (CardStatusClosed == card.status) {
        card.status = CardStatusOpened;
        [_openedCards addObject:card];
        
        _points--;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CardGamePointsDidUpdatedNotification
                                                            object:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CardGameCardDidOpenedNotification
                                                            object:@(index)];
        
        if (_openedCards.count == [self cardsToMatchCount]) {
            [self checkForMatching];
        }
    }
}

- (PlayingCard *)openedCardWithIndex:(NSUInteger)index
{
    assert(index < _cards.count);
    
    PlayingCardInGame *card = _cards[index];
    
    assert(CardStatusOpened == card.status);
    
    return card.card;
}

@end
