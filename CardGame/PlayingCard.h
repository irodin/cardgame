//
//  PlayingCard.h
//  CardGame
//
//  Created by Ilya Rodin on 19.03.14.
//  Copyright (c) 2014 IlyaRodin. All rights reserved.
//

#import <Foundation/Foundation.h>
        
typedef NS_ENUM(NSUInteger, CardSuit) {
    CardSuitClubs,
    CardSuitDiamonds,
    CardSuitHearts,
    CardSuitSpades
};

typedef NS_ENUM(NSUInteger, CardRank) {
    CardRank2 = 2,
    CardRank3,
    CardRank4,
    CardRank5,
    CardRank6,
    CardRank7,
    CardRank8,
    CardRank9,
    CardRank10,
    CardRankJack,
    CardRankQueen,
    CardRankKing,
    CardRankAce
};

@interface PlayingCard : NSObject

@property (nonatomic, readonly) CardSuit suit;
@property (nonatomic, readonly) CardRank rank;
@property (nonatomic, readonly) NSString *spriteImageName;

- (id)initWithSuit:(CardSuit)suit rank:(CardRank)rank;

+ (NSArray *)allCards;

+ (NSString *)closedCardSpriteImageName;

@end
