//
//  AppDelegate.h
//  CardGame
//
//  Created by Ilya Rodin on 19.03.14.
//  Copyright (c) 2014 IlyaRodin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
