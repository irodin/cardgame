//
//  MyScene.m
//  CardGame
//
//  Created by Ilya Rodin on 19.03.14.
//  Copyright (c) 2014 IlyaRodin. All rights reserved.
//

#import "MyScene.h"
#import "CardGame.h"
#import "PlayingCard.h"

static const NSUInteger kCardsInRowCount = 6;
static const CGFloat kCardHideDelay = 1.0;

@interface MyScene () {
    SKLabelNode *_restartButton;
    SKLabelNode *_gameTypeSwitch;
    SKLabelNode *_pointsLabel;
    
    NSMutableArray *_cardSprites;
}

@end

@implementation MyScene

#pragma mark - Init & dealloc

- (id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size]) {
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        
        // restart button (as a label)
        _restartButton = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
        _restartButton.fontSize = 20;
        _restartButton.position = CGPointMake(70, 700);
        _restartButton.text = @"Restart";
        [self addChild:_restartButton];
        
        // game type switch (2 or 3 cards) (as a label)
        _gameTypeSwitch = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
        _gameTypeSwitch.fontSize = 20;
        _gameTypeSwitch.position = CGPointMake(350, 700);
        [self addChild:_gameTypeSwitch];
        
        // points label
        _pointsLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
        _pointsLabel.fontSize = 20;
        _pointsLabel.position = CGPointMake(CGRectGetWidth(self.frame) - 100, 700);
        [self addChild:_pointsLabel];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onCardsGiven)
                                                     name:CardGameCardsWereGivenNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onPointsDidUpdated)
                                                     name:CardGamePointsDidUpdatedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onCardDidOpened:)
                                                     name:CardGameCardDidOpenedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onCardsDidClosed:)
                                                     name:CardGameCardsDidClosedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onCardsDidMatched:)
                                                     name:CardGameCardsDidMatchedNotification
                                                   object:nil];
        
        [CardGame sharedInstance].gameType = CardGameType2Cards;
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        // touch restart button?
        if (CGRectContainsPoint(_restartButton.frame, location)) {
            [[CardGame sharedInstance] restart];
            return;
        }
        
        // change game type?
        if (CGRectContainsPoint(_gameTypeSwitch.frame, location)) {
            if (CardGameType2Cards == [CardGame sharedInstance].gameType) {
                [CardGame sharedInstance].gameType = CardGameType3Cards;
            } else {
                [CardGame sharedInstance].gameType = CardGameType2Cards;
            }
            return;
        }
        
        // touch card?
        for (SKSpriteNode *cardSprite in _cardSprites) {
            if (CGRectContainsPoint(cardSprite.frame, location)) {
                [[CardGame sharedInstance] openCardWithIndex:[_cardSprites indexOfObject:cardSprite]];
                break;
            }
        }
    }
}

#pragma mark - GUI

- (void)createCardsSprites
{
    assert([NSThread isMainThread]);
    
    if (nil == _cardSprites) {
        _cardSprites = [[NSMutableArray alloc] initWithCapacity:[[CardGame sharedInstance] givenCardsCount]];
    } else {
        [self removeChildrenInArray:_cardSprites];
        [_cardSprites removeAllObjects];
    }
    
    NSMutableArray *cardSpritesRow2 = [[NSMutableArray alloc] initWithCapacity:kCardsInRowCount];
    
    CGFloat deltaHorizontal = CGRectGetWidth(self.frame) / 6;
    for (NSUInteger i = 0; i < kCardsInRowCount; i++) {
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:[PlayingCard closedCardSpriteImageName]];
        sprite.position = CGPointMake(deltaHorizontal * i + 61.5, 500); // 83 / 2 = 61.5
        sprite.size = CGSizeMake(83, 120);
        [self addChild:sprite];
        
        [_cardSprites addObject:sprite];
        
        sprite = [SKSpriteNode spriteNodeWithImageNamed:[PlayingCard closedCardSpriteImageName]];
        sprite.position = CGPointMake(deltaHorizontal * i + 61.5, 360);
        sprite.size = CGSizeMake(83, 120);
        [self addChild:sprite];
        
        [cardSpritesRow2 addObject:sprite];
    }
    
    for (SKSpriteNode *spriteFromRow2 in cardSpritesRow2) {
        [_cardSprites addObject:spriteFromRow2];
    }
}

- (void)showOpenedCardWithIndex:(NSUInteger)index
{
    assert([NSThread isMainThread]);
    
    SKSpriteNode *oldSprite = [_cardSprites objectAtIndex:index];
    
    PlayingCard *openedCard = [[CardGame sharedInstance] openedCardWithIndex:index];
    
    SKSpriteNode *newSprite = [SKSpriteNode spriteNodeWithImageNamed:openedCard.spriteImageName];
    newSprite.position = oldSprite.position;
    newSprite.size = oldSprite.size;
    
    [self removeChildrenInArray:@[oldSprite]];

    _cardSprites[index] = newSprite;
    
    [self addChild:newSprite];
}

- (void)hideOpenedCardsWithIndices:(NSArray *)indices
{
    assert([NSThread isMainThread]);
    
    for (NSNumber *ind in indices) {
        SKSpriteNode *oldSprite = [_cardSprites objectAtIndex:[ind unsignedIntegerValue]];
        
        SKSpriteNode *newSprite = [SKSpriteNode spriteNodeWithImageNamed:[PlayingCard closedCardSpriteImageName]];
        newSprite.position = oldSprite.position;
        newSprite.size = oldSprite.size;
        
        [self removeChildrenInArray:@[oldSprite]];

        _cardSprites[[ind unsignedIntegerValue]] = newSprite;
        
        [self addChild:newSprite];
    }
}

- (void)markAsMatchedOpenedCardWithIndices:(NSArray *)indices
{
    assert([NSThread isMainThread]);
    
    for (NSNumber *ind in indices) {
        SKSpriteNode *sprite = [_cardSprites objectAtIndex:[ind unsignedIntegerValue]];
        sprite.hidden = YES;
    }
}

- (void)updateGameTypeInfo
{
    assert([NSThread isMainThread]);
    
    if (CardGameType2Cards == [CardGame sharedInstance].gameType) {
        _gameTypeSwitch.text = @"Switch to 3 cards (now 2 cards)";
    } else { // 3 cards
        _gameTypeSwitch.text = @"Switch to 2 cards (now 3 cards)";
    }
}

- (void)updatePointsInfo
{
    assert([NSThread isMainThread]);
    
    _pointsLabel.text = [NSString stringWithFormat:@"Points: %ld", (long)[CardGame sharedInstance].points];
}

#pragma mark - Notifications' catchers

- (void)onCardsGiven
{
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self onCardsGiven];
        });
        return;
    }
    
    [self createCardsSprites];
    
    [self updateGameTypeInfo];
}

- (void)onPointsDidUpdated
{
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self onPointsDidUpdated];
        });
        return;
    }
    
    [self updatePointsInfo];
}

- (void)onCardDidOpened:(NSNotification *)nt
{
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self onCardDidOpened:nt];
        });
        return;
    }
    
    NSUInteger openedInd = [(NSNumber *)nt.object unsignedIntegerValue];
    
    [self showOpenedCardWithIndex:openedInd];
}

- (void)onCardsDidClosed:(NSNotification *)nt
{
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self onCardsDidClosed:nt];
        });
        return;
    }
    
    NSArray *indicesToClose = (NSArray *)nt.object;
    
    [self performSelector:@selector(hideOpenedCardsWithIndices:)
               withObject:indicesToClose
               afterDelay:kCardHideDelay];
}

- (void)onCardsDidMatched:(NSNotification *)nt
{
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self onCardsDidMatched:nt];
        });
        return;
    }
    
    NSArray *indicesToMatch = (NSArray *)nt.object;
    
    [self performSelector:@selector(markAsMatchedOpenedCardWithIndices:)
               withObject:indicesToMatch
               afterDelay:kCardHideDelay];
}

@end
