//
//  PlayingCardInGame.m
//  CardGame
//
//  Created by Ilya Rodin on 19.03.14.
//  Copyright (c) 2014 IlyaRodin. All rights reserved.
//

#import "PlayingCardInGame.h"

@implementation PlayingCardInGame

- (id)initWithCard:(PlayingCard *)card
{
    if (self = [super init]) {
        _card = card;
        _status = CardStatusClosed;
    }
    
    return self;
}

@end
