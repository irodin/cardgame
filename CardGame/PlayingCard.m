//
//  PlayingCard.m
//  CardGame
//
//  Created by Ilya Rodin on 19.03.14.
//  Copyright (c) 2014 IlyaRodin. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

- (id)initWithSuit:(CardSuit)suit rank:(CardRank)rank
{
    if (self = [super init]) {
        _suit = suit;
        _rank = rank;
        _spriteImageName = [[self class] spriteImageNameForSuit:suit
                                                           rank:rank];
    }
    
    return self;
}

+ (NSString *)spriteImageNameForSuit:(CardSuit)suit rank:(CardRank)rank
{
    NSMutableString *imageName;
    
    switch (rank) {
        case CardRankAce:
            imageName = [@"ace_of_" mutableCopy];
            break;
        case CardRankKing:
            imageName = [@"king_of_" mutableCopy];
            break;
        case CardRankQueen:
            imageName = [@"queen_of_" mutableCopy];
            break;
        case CardRankJack:
            imageName = [@"jack_of_" mutableCopy];
            break;
        default:
            imageName = [[NSString stringWithFormat:@"%lu_of_", (unsigned long)rank] mutableCopy];
            break;
    }
    
    switch (suit) {
        case CardSuitClubs:
            [imageName appendString:@"clubs"];
            break;
        case CardSuitDiamonds:
            [imageName appendString:@"diamonds"];
            break;
        case CardSuitHearts:
            [imageName appendString:@"hearts"];
            break;
        case CardSuitSpades:
            [imageName appendString:@"spades"];
            break;
    }
    
    return imageName;
}

#pragma mark - Class methods

+ (NSArray *)allCards
{
    NSArray *allSuits = @[@(CardSuitClubs), @(CardSuitDiamonds), @(CardSuitHearts), @(CardSuitSpades)];
    NSArray *allRanks = @[@(CardRank2), @(CardRank3), @(CardRank4), @(CardRank5), @(CardRank6), @(CardRank7), @(CardRank8), @(CardRank9), @(CardRank10), @(CardRankJack), @(CardRankQueen), @(CardRankKing), @(CardRankAce)];
    
    NSMutableArray *allCards = [[NSMutableArray alloc] initWithCapacity:allSuits.count * allRanks.count];
    
    for (NSUInteger i = 0; i < allSuits.count; i++) {
        for (NSUInteger j = 0; j < allRanks.count; j++) {
            CardSuit suit = [(NSNumber *)allSuits[i] unsignedIntegerValue];
            CardRank rank = [(NSNumber *)allRanks[j] unsignedIntegerValue];
            PlayingCard *card = [[PlayingCard alloc] initWithSuit:suit rank:rank];
            [allCards addObject:card];
        }
    }
    
    return allCards;
}

+ (NSString *)closedCardSpriteImageName
{
    return @"shirt";
}

@end
