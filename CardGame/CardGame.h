//
//  CardGame.h
//  CardGame
//
//  Created by Ilya Rodin on 19.03.14.
//  Copyright (c) 2014 IlyaRodin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const CardGamePointsDidUpdatedNotification;
extern NSString *const CardGameCardsWereGivenNotification;
extern NSString *const CardGameCardDidOpenedNotification;
extern NSString *const CardGameCardsDidMatchedNotification;
extern NSString *const CardGameCardsDidClosedNotification;

typedef NS_ENUM(NSUInteger, CardGameType) {
    CardGameType2Cards,
    CardGameType3Cards
};

typedef NS_ENUM(NSUInteger, CardStatus) {
    CardStatusClosed = 0,
    CardStatusOpened,
    CardStatusMatched
};

@class PlayingCard;

@interface CardGame : NSObject

@property (nonatomic, readonly, assign) NSInteger points;

@property (nonatomic, assign) CardGameType gameType;

+ (CardGame *)sharedInstance;

- (void)restart;

- (NSUInteger)givenCardsCount;

- (CardStatus)statusOfCardWithIndex:(NSUInteger)index;

- (void)openCardWithIndex:(NSUInteger)index;

- (PlayingCard *)openedCardWithIndex:(NSUInteger)index;

@end
